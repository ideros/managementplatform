var express = require('express');
var router = express.Router();
var Everlive = require('everlive-sdk');
var el = require('./single').single.el;

router.use(function(req, res, next){
    if (req.session.authToken){
        console.log("Logged in");
        el = new Everlive({apiKey: 'Xi7yEjFuv1A5vbBO', token: req.session.authToken});
    } else {
        console.log("Not logged in");
    }

    next();
});
router.use('/pages/home', require('./home'));
router.use('/pages/events', require('./events'));
router.use('/pages/account', require('./account'));
router.use('/api', require('./api'));

/* GET home page. */
router.get('/', function(req, res, next) {

  if (req.session.authToken){
    res.render('index');
  } else {
    // TODO: session expired
    res.render('login');
  }
});
/* GET login page. */
router.get('/login', function(req, res, next) {
  res.render('login');
});
// Log in
router.post('/login', function(req, resp, next){
  var email = req.body.email;
  var pwd = req.body.password;

  el.Users.login(email, pwd,
    function(data){
      req.session.authToken = data.result.access_token;
      resp.redirect('/');
    },
    function(error){
      console.log(JSON.stringify(error));
      resp.render('login', { authError: error.message, email: email });
    });
});

// Log out
router.get('/logout', function(req, res){
    el.Users.logout().then(function(){
        req.session.authToken = null;
        res.redirect('/');
    });
});

// Log in using Facebook
router.post('/loginWithFacebook', function(req, resp, next){
    var accessToken = req.body.authToken;

    el.Users.loginWithFacebook(accessToken,
        function(data){
            req.session.authToken = data.result.access_token;
            req.session.fbToken = accessToken;
            resp.redirect("/");
        },
        function(error){
            console.log(JSON.stringify(error));
            resp.render('login', { authError: error.message });
        });
});

// Register an account
router.post('/register', function(req, res){
    var username = req.body.email;
    var password = req.body.password;

    el.Users.register(username, password, {/* other attributes */},
        function(data){
            res.json({'data' : data});
        },
        function(error){
            res.json({'data' : error});
        });
});

module.exports = router;
