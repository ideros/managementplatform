var express = require('express');
var router = express.Router();
var el = require('./single').single.el;

router.get('/', function(req, res){
    res.render('pages/events', { fbToken : req.session.fbToken });
});

router.get('/create', function(req, res){
    res.render('pages/createEvent');
});

router.get('/link', function(req, res){
    res.render('pages/link');
});

module.exports = router;
