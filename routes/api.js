var express = require('express');
var router = express.Router();
var Everlive = require('everlive-sdk');
var el = require('./single').single.el;
var request = require('request');
var cloudinary = require('cloudinary');
/*
 * all data requests are handled inside this API
 *
 * contents :
 *
 * CLOUDINARY
 * USER
 * ARTISTS
 * VENUES
 * ACCOUNT
 * EVENTS
 * MIXTRACKS
 * PAGES 
 */

// CLOUDINARY ******************************************************************
var arrTimeoutsBeforeDelete = [];
var arrEventsIdIndex = [];
// array of event [eventId][photoDeleteTimeouts]
cloudinary.config({
    cloud_name: 'appolomusic',
    api_key: '687224137781455',
    api_secret: '-hS_iPUfsOHmS9WHob3OHnYjNn4'
});
/* upload an image
* @param req.body.url image to upload
* @param req.body.eventId id of the event containing image
*/
router.post('/upload', function(req, res){
    cloudinary.uploader.upload(req.body.url, function(result) {
        var public_id = result.public_id;
        res.json({ 'id': public_id });

        var i = arrEventsIdIndex.indexOf(req.body.eventId);

        if (i == -1){
            i = arrEventsIdIndex.push(req.body.eventId) - 1; // push returns new array length
            arrTimeoutsBeforeDelete.push([]);
        }

        // remove photo from cloudinary (only if event is not saved within a reasonable delay)
        var timeout = setTimeout(function(){
            cloudinary.api.delete_resources([public_id],
                function(result){
                    console.log(result.deleted);
                });
            // remove all timeouts for this event when expired
            var eventIndex = arrEventsIdIndex.indexOf(req.body.eventId);
            // timeout index may already have been removed by a previous one
            if (!!~eventIndex){
                arrEventsIdIndex.splice(eventIndex, 1);
                arrTimeoutsBeforeDelete.splice(eventIndex, 1);
            }
        }, 1000 * 60 * 60 * 12);    // photo will be deleted after 12 hours

        arrTimeoutsBeforeDelete[i].push(timeout);
    });
});

router.post('/delete/images', function(req, res){
    // delete cloudinary images
    if (req.body.cloudinaryImageIds && req.body.cloudinaryImageIds.length > 0){
        cloudinary.api.delete_resources(req.body.cloudinaryImageIds, function(result){
            console.log(result.deleted);
        });
    }
    // delete everlive images
    if (req.body.everliveImageIds && req.body.everliveImageIds.length > 0){
        var filesUrl = 'http://api.everlive.com/v1/Xi7yEjFuv1A5vbBO/Files';
        var filter = {"Id" : {"$in" : req.body.everliveImageIds}};
        var authorizationHeaders = {
            "Authorization" : "Bearer " + req.session.authToken,
            "X-Everlive-Filter" : JSON.stringify(filter)
        };

        request({ url: filesUrl, method: 'DELETE', headers: authorizationHeaders },
            function (error, response, body) {
                if (response.statusCode == 200) {
                    console.log('Images successfully deleted from everlive');
                } else {
                    console.log('Error deleting images from everlive database');
                }
            });
    }
});

/*
* USER ************************************************************************
*/
// GET logged in user data
router.get('/user', function(req, res){
    el.Users.currentUser().then(function(data){
        res.json(data.result);
    },function(error){
        res.redirect('/login', {authError: error.message});
    });
});

// POST update user linked artist profile
router.post('/user/artist', function(req, res){
    el.Users.updateSingle({'Id' : req.body.id, 'Artist' : req.body.artist},
        function(data){
            res.json({'status' : 'success'});
        }),
        function(error){
            res.json({'status' : 'error'});
        };
});
/*
* ARTISTS **********************************************************************
*/
router.get('/artist/:id', function(req, res){
    var artistsUrl = 'http://api.everlive.com/v1/Xi7yEjFuv1A5vbBO/Artists';
    var filter = {"Id" : req.params.id}
    var authorizationHeaders = {
        "Authorization" : "Bearer " + req.session.authToken,
        "X-Everlive-Filter" : JSON.stringify(filter)
    };

    request({ url: artistsUrl, method: 'GET', headers: authorizationHeaders }, function (error, response, body) {
        if (response.statusCode == 200) {
            res.json(body);
        } else {
            res.json({'error' : 'No artist found with the specified Id.'})
        }
    });
});
// GET a list containing the names of all artists (format : {Name : "name", Id : "id"}
router.get('/artistsNamesList/search=:name', function (req, res) {
    var artistsUrl = 'http://api.everlive.com/v1/Xi7yEjFuv1A5vbBO/Artists';
    var fieldsExpected = { "Name" : 1 }; // retrieve only Id and Name.
    var filter = {"Name" : {"$regex" : '.*' + req.params.name + '.*', "$options" : "si"}}
    var sortExp = {"Name" : 1};
    var authorizationHeaders = {
        "Authorization" : "Bearer " + req.session.authToken,
        "X-Everlive-Fields" : JSON.stringify(fieldsExpected),
        "X-Everlive-Filter" : JSON.stringify(filter),
        "X-Everlive-Sort" : JSON.stringify(sortExp)
    };

    request({ url: artistsUrl, method: 'GET', headers: authorizationHeaders }, function (error, response, body) {
        if (response.statusCode == 200) {
            res.json(body);
        } else {
            console.log('error retrieving the artist names list');
        }
    });
});

router.post('/artist/create', function(req, res){
    var artist = req.body.artistData;
    
    artist && cloudinary.uploader.upload(artist.ProfilePicLink, function(result) {
        var data = el.data('Artists');
        artist.ProfilePicLink = result.url;     
                            
        console.log("cloudinary result : " + JSON.stringify(result));                            
        console.log("uploading artist : " + JSON.stringify(artist));   

        data.create(artist, function(response){
                res.json({'Status' : 'success'});
                console.log("success response: " + JSON.stringify(response));
            },
            function(error){
                res.json({'Status' : 'error'});
                console.log("success error: " + JSON.stringify(response));
            }
        );
    });
});

/*
* VENUES ***********************************************************************
*/
// GET a list containing the names of all venues (format : {Name : "name", Id : "id"}
router.get('/venuesNamesList/search=:name', function (req, res) {
    var artistsUrl = 'http://api.everlive.com/v1/Xi7yEjFuv1A5vbBO/Venues';
    var fieldsExpected = { "Name" : 1 }; // retrieve only Id and Name.
    var filter = {"Name" : {"$regex" : '.*' + req.params.name + '.*', "$options" : "si"}}
    var sortExp = {"Name" : 1};
    var authorizationHeaders = {
        "Authorization" : "Bearer " + req.session.authToken,
        "X-Everlive-Fields" : JSON.stringify(fieldsExpected),
        "X-Everlive-Filter" : JSON.stringify(filter),
        "X-Everlive-Sort" : JSON.stringify(sortExp)
    };

    request({ url: artistsUrl, method: 'GET', headers: authorizationHeaders }, function (error, response, body) {
        if (response.statusCode == 200) {
            res.json(body);
        } else {
            console.log('error retrieving the venues names list');
        }
    });
});

router.get('/venue/:id', function(req, res){
    var venues = 'http://api.everlive.com/v1/Xi7yEjFuv1A5vbBO/Venues';
    var filter = {"Id" : req.params.id}
    var authorizationHeaders = {
        "Authorization" : "Bearer " + req.session.authToken,
        "X-Everlive-Filter" : JSON.stringify(filter)
    };

    request({ url: venues, method: 'GET', headers: authorizationHeaders }, function (error, response, body) {
        if (response.statusCode == 200) {
            res.json(body);
        } else {
            res.json({'error' : 'No venue found with the specified Id.'})
        }
    });
});
/*
* ACCOUNT **********************************************************************
*/
router.post('/account/update', function(req, res){
    el.Users.updateSingle(req.body.accountData,
        function(data){
            res.json({'Status' : 'success'});
        },
        function(error){
            res.json({'Status' : 'error', 'error' : error});
        });
});

router.post('/account/changePassword', function(req, res){
    el.Users.changePassword(req.body.username,
        req.body.currentPassword,
        req.body.password,
        true,
        function(data){
            res.send(data);
        },
        function(error){
            res.send(error);
        });
});

router.get('/artist/userlink/:id', function(req, res){
    var artistId = req.params.id;
    var usersUrl = 'http://api.everlive.com/v1/Xi7yEjFuv1A5vbBO/Users';

    var fieldsExpected = { "Id" : 1 }; // retrieve only Id
    var filter = { "Artist" : artistId }
    var authorizationHeaders = {
        "Authorization" : "Bearer " + req.session.authToken,
        "X-Everlive-Fields" : JSON.stringify(fieldsExpected),
        "X-Everlive-Filter" : JSON.stringify(filter)
    };

    request({ url: usersUrl, method: 'GET', headers: authorizationHeaders }, function (error, response, body) {
        if (response.statusCode == 200) {
            res.json(body);
        } else {
            res.json({'error' : 'No venue found with the specified Id.'})
        }
    });
});
/*
* EVENTS ***********************************************************************
*/
// GET events data
router.get('/events/:artistId', function(req, res){
    var eventsUrl = 'http://api.everlive.com/v1/Xi7yEjFuv1A5vbBO/Events';
    var authorizationHeaders = {
        "Authorization" : "Bearer " + req.session.authToken,
        "X-Everlive-Filter" : JSON.stringify({"Artists" : req.params.artistId})
    };

    request({url: eventsUrl, method: 'GET', headers: authorizationHeaders}, function(error, response, body){
        if (response.statusCode == 200){
            res.json(body);
        } else {
            res.json({error: "An error occured while accessing the events data. Please try again later"});
        }
    });
});

// GET some of a specific event's data
router.get('/event/:id', function(req, res){
    var eventsUrl = 'http://api.everlive.com/v1/Xi7yEjFuv1A5vbBO/Events';
    var filter = {"Id" : req.params.id}
    var expand = { "Artists" : true, "Venue" : true };
    var authorizationHeaders = {
        "Authorization" : "Bearer " + req.session.authToken,
        "X-Everlive-Filter" : JSON.stringify(filter),
        "X-Everlive-Expand": JSON.stringify(expand)
    };

    request({ url: eventsUrl, method: 'GET', headers: authorizationHeaders }, function (error, response, body) {
        if (response.statusCode == 200) {
            res.json(body);
        } else {
            res.json({'error' : 'No event found with the specified Id.'})
        }
    });
});

// GET an event's images
router.get('/event/images/:id', function(req, res){
    var eventsUrl = 'http://api.everlive.com/v1/Xi7yEjFuv1A5vbBO/Events';
    var filter = {"Id" : req.params.id};
    var expand = { "Image" : true, "Photos" : true};
    var fieldsExpected = { "Id" : 0, "Image" : 1, "Photos" : 1 };
    var authorizationHeaders = {
        "Authorization" : "Bearer " + req.session.authToken,
        "X-Everlive-Fields" : JSON.stringify(fieldsExpected),
        "X-Everlive-Filter" : JSON.stringify(filter),
        "X-Everlive-Expand": JSON.stringify(expand)
    };

    request({ url: eventsUrl, method: 'GET', headers: authorizationHeaders }, function (error, response, body) {
        if (response.statusCode == 200) {
            res.json(body);
        } else {
            res.json({'error' : 'No images found for the specified event Id'})
        }
    });
});
// UPDATE an event
router.post('/event/:id', function(req, res){
    var event = req.body.event;

    if (event){
        var data = el.data('Events');
        var values = {
            'Id' : req.params.id,
            'Description': event.Description,
            'Date' : event.Date,
            'End' : event.End,
            'Cover' : event.Cover,
            'Name' : event.Name,
            'Place' : event.Place,
            'Address' : event.Address,
            'Venue' : event.Venue,
            'Website' : event.Website,
            'Artists' : event.Artists,
            'Fbfeed' : event.Fbfeed,
            'Igfeed' : event.Igfeed,
            'Twfeed' : event.Twfeed,
            'Photos_Url' : event.Photos_Url
        }

        data.updateSingle(values, function(response){
                var indexEvent = arrEventsIdIndex.indexOf(req.params.id);

                // clear timeouts for this event
                if (!!~indexEvent){
                    for (var i = arrTimeoutsBeforeDelete[indexEvent].length;i-- > 0;){
                        clearTimeout(arrTimeoutsBeforeDelete[indexEvent][i]);
                    }
                    arrTimeoutsBeforeDelete.splice(indexEvent, 1);
                    arrEventsIdIndex.splice(indexEvent, 1);
                }

                res.json({'Status' : 'success'});
            },
            function(error){
                res.json({'Status' : 'failure'});
            }
        );
    }
});

router.post('/events/create', function(req, res){
    var event = req.body.event;

    if (event){
        var data = el.data('Events');
        var values = {
            'Description': event.Description,
            'Date' : event.Date,
            'End' : event.End,
            'Cover' : event.Cover,
            'Name' : event.Name,
            'Place' : event.Place,
            'Venue' : event.Venue,
            'Website' : event.Website,
            'Address' : event.Address,
            'Artists' : event.Artists,
            'Fbfeed' : event.Fbfeed,
            'Igfeed' : event.Igfeed,
            'Twfeed' : event.Twfeed,
            'Photos_Url' : event.Photos_Url
        }

        data.create(values, function(response){
                res.json({'Status' : 'success'});
            },
            function(error){
                res.json({'Status' : 'failure'});
            }
        );
    }
});
/**
* Evaluate which facebook event is currently synced in FBToScrape
*/
router.post("/events/findSynced", function(req, res){
    var ids = req.body.FBEventsIds;
    var FBToScrapeURL = 'http://api.everlive.com/v1/Xi7yEjFuv1A5vbBO/FBToScrape';
    var filter = {"FBID" : {"$in" : ids}, "Type" : "EventID"};
    var fieldsExpected = { "Id" : 0, "Sync" : 1 };
    var authorizationHeaders = {
        "Authorization" : "Bearer " + req.session.authToken,
        "X-Everlive-Fields" : JSON.stringify(fieldsExpected),
        "X-Everlive-Filter" : JSON.stringify(filter)
    };


    request({ url: FBToScrapeURL, method: 'GET', headers: authorizationHeaders }, function(error, response, body){
        if (response.statusCode == 200){
            res.json({'Status' : 'success', 'pages' : JSON.parse(body).Result });
        } else {
            res.json({'Status' : 'error'});
        }
    });
});

router.post('/events/filterSynced', function(req, res){
    var FBToScrapeURL = 'http://api.everlive.com/v1/Xi7yEjFuv1A5vbBO/FBToScrape';
    var filter = {"FBID" : {"$in" : req.body.ids}, "Sync" : true, "Type" : "EventID"};
    var fieldsExpected = { "Id" : 0, "FBID" : 1};
    var authorizationHeaders = {
        "Authorization" : "Bearer " + req.session.authToken,
        "X-Everlive-Fields" : JSON.stringify(fieldsExpected),
        "X-Everlive-Filter" : JSON.stringify(filter)
    };

    request({ url: FBToScrapeURL, method: 'GET', headers: authorizationHeaders }, function(error, response, body){
        if (response.statusCode == 200){
            body = JSON.parse(body);
            res.json(body.Result);
        } else {
            res.json({'Status' : 'error'});
        }
    });
});

/*
* untested...
* Unsync an event by setting it's "Sync" status to false and removing Manager ID
*/
router.post('/event/unsync', function(req, res){
    var eventId = req.params.eventId;

    var FBToScrapeURL = 'http://api.everlive.com/v1/Xi7yEjFuv1A5vbBO/FBToScrape';
    var filter = {"FBID" : pageId, "Type" : 'EventID'}
    var data = el.data('FBToScrape');

    data.update({"Sync" : false, "Manager" : null}, filter, function(response){
            res.json({'Status' : 'success'});
        },
        function(error){
            res.json({'Status' : 'failure'});
        }
    );
});
// MIXTRACKS *******************************************************************
/*
* upload a mixtrack for an event
*/
router.post('/mixtrack/upload', function(req, res){
    var file = req.body[0].file;
    var dropboxAccessToken = "T-5YtdlFJA4AAAAAAAAThedmsK35K47SDAJ0k1SWcHSThCee7JfC9tAR_0oExRXF";
    var filename = 'fromMP';
    var dropboxFileputUrl = 'https://api-content.dropbox.com/1/files_put/auto/public/mixtracks/' + filename;

    console.log('Sending file : ' + JSON.stringify(file));

    request({ url: dropboxFileputUrl, method: 'POST',
        headers: {"Authorization" : "Bearer " + dropboxAccessToken} }, function(error, response, body){
        if (response.statusCode == 200){
            body = JSON.parse(body);
            res.json(body.Result);
        } else {
            res.json({'Status' : 'error'});
        }
    });
});
// PAGES ***********************************************************************
/*
* get all pages of currently connected user
*/
router.get('/pages/:userId', function(req, res){
    var userId = req.params.userId;
    var FBToScrapeURL = 'http://api.everlive.com/v1/Xi7yEjFuv1A5vbBO/FBToScrape';
    var filter = {"Manager" : userId, "Type" : "PageID"};
    var fieldsExpected = { "Id" : 0, "FBID" : 1, "Sync" : 1 };
    var authorizationHeaders = {
        "Authorization" : "Bearer " + req.session.authToken,
        "X-Everlive-Fields" : JSON.stringify(fieldsExpected),
        "X-Everlive-Filter" : JSON.stringify(filter)
    };


    request({ url: FBToScrapeURL, method: 'GET', headers: authorizationHeaders }, function(error, response, body){
        if (response.statusCode == 200){
            res.json({'Status' : 'success', 'pages' : JSON.parse(body).Result });
        } else {
            res.json({'Status' : 'error'});
        }
    });
});
/**
* unsync items from FBToScrape with specified IDs
*/
router.post('/pages/unsync', function(req, res){
    var userId = req.body.userId;
    var ids = req.body.ids;
    var data = el.data('FBToScrape');
    var filter = {"FBID" : {"$in" : ids}};
    var values = {
        'Manager' : userId,
        'Sync' : false
    }

    data.update(values, filter, function(response){
            res.json({'Status' : 'success'});
        },
        function(error){
            res.json({'Status' : 'failure'});
        }
    );
});
/**
* sync items from FBToScrape with specified IDs
* if item does not already exists, it is instead created.
*/
router.post('/pages/sync', function(req, res){
    var userId = req.body.userId;
    var ids = req.body.ids;
    var data = el.data('FBToScrape');
    var filter = {"FBID" : {"$in" : ids}};
    var values = {
        'Manager' : userId,
        'Sync' : true
    }
    // ****** UPDATE ITEMS *******
    data.update(values, filter, function(response){
            done();
        },
        function(error){
            res.json({'Status' : 'failure'});
        }
    );
    // ****** CREATE EVENTS *******
    // get fbids that are in "ids", from database
    var fieldsExpected = { "Id" : 0, "FBID" : 1 };
    var authorizationHeaders = {
        "Authorization" : "Bearer " + req.session.authToken,
        "X-Everlive-Fields" : JSON.stringify(fieldsExpected),
        "X-Everlive-Filter" : JSON.stringify(filter)
    };
    var FBToScrapeURL = 'http://api.everlive.com/v1/Xi7yEjFuv1A5vbBO/FBToScrape';
    request({ url: FBToScrapeURL, method: 'GET', headers: authorizationHeaders }, function(error, response, body){
        if (response.statusCode == 200){

            // get fbids that are not in database
            body = JSON.parse(body);
            var index;
            var createIds = ids;
            body.Result.forEach(function(FBToScrapeUpdatedObject){
                if ((index = createIds.indexOf(FBToScrapeUpdatedObject.FBID)) != -1){
                    createIds.splice(index, 1);
                }
            });

            // prepare data for create query (fbids that are not in database already)
            var createData = [];
            createIds.forEach(function(fbid){
                createData.push({
                    'FBID' : fbid,
                    'Type' : "EventID", // pages are created when selecting them, so only events need to be created on sync
                    'Manager' : userId,
                    'Sync' : true
                });
            });

            // create query
            data.create(createData,
                function(data){
                    done();
                },
                function(error){
                    res.json({'Status' : 'error'});
                });
        } else {
            res.json({'Status' : 'error'});
        }
    });

    var doneCount = 0;
    var done = function(){
        doneCount++;
        // if update task + create task are done
        if (doneCount == 2){
            res.json({'Status' : 'success'});
        }
    }
});
/*
* If not managed already, logged in user becomes page manager
*/
router.post('/page/manage/:pageid', function(req, res){
    var pageId = req.params.pageid;
    var userId = req.body.userId;

    var FBToScrapeURL = 'http://api.everlive.com/v1/Xi7yEjFuv1A5vbBO/FBToScrape';
    var filter = {"FBID" : pageId, "Type" : 'PageID'}
    var authorizationHeaders = {
        "Authorization" : "Bearer " + req.session.authToken,
        "X-Everlive-Filter" : JSON.stringify(filter)
    };

    request({ url: FBToScrapeURL, method: 'GET', headers: authorizationHeaders }, function (error, response, body) {
        if (response.statusCode == 200) {
            body = JSON.parse(body);
            var pageExisting = body.Count;

            if (pageExisting){
                if (body.Result[0].Manager){
                    if (body.Result[0].Manager == userId){
                        res.json({ 'Status' : 'success', 'page' : 'existing', 'manager' : 'self' });
                    } else {
                        res.json({ 'Status' : 'success', 'page' : 'existing', 'manager' : 'other' });
                    }
                } else {
                    var data = el.data('FBToScrape');
                    var value = {
                        'Id' : body.Result[0].Id,
                        'Manager' : userId
                    }

                    data.updateSingle(value, function(response){
                            res.json({'Status' : 'success', 'page' : 'existing'});
                        },
                        function(error){
                            res.json({'Status' : 'failure'});
                        }
                    );
                }
            } else {
                var data = el.data('FBToScrape');
                var values = {
                    'Manager' : userId,
                    "FBID" : pageId,
                    "Type" : 'PageID'
                };

                data.create(values, function(response){
                        res.json({'Status' : 'success', 'page' : 'created'});
                    },
                    function(error){
                        res.json({'Status' : 'failure'});
                    }
                );
            }
        } else {
            res.json({'Status' : 'error', 'error' : 'Unable to access Pages'})
        }
    });
});

/*
* Unmanaging a page undefines it's Manager and sets sync to false, as well
* as it's events
*/
router.post('/page/unmanage/:pageId', function(req, res){
    var pageId = req.params.pageId;
    var ids = req.body.eventFBIDs;
    ids.push(pageId);
    var data = el.data('FBToScrape');
    var filter = {
        "FBID" : {"$in" : ids}
    }
    var value = {
        'Manager' : null,
        'Sync' : false
    }

    data.update(value, filter, function(response){
            res.json({'Status' : 'success'});
        },
        function(error){
            res.json({'Status' : 'failure'});
        }
    );
});

module.exports = router;
