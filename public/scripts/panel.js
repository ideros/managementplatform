var mpApp = angular.module('ManagementPlatformApp', ['ui.router',
    'autocomplete', 'ngFileUpload']);

mpApp.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider){
    $urlRouterProvider.otherwise('/');

    $stateProvider
        .state('home', {
            url: '/',
            templateUrl: 'pages/home',
            controller: 'homeController'
        }).state('events', {
            url: '/events',
            templateUrl: 'pages/events',
            controller: 'eventsController'
        }).state('createEvent', {
            url: '/events/create',
            templateUrl: 'pages/events/create',
            controller: 'eventsController'
        }).state('account', {
            url: '/account',
            templateUrl: 'pages/account',
            controller: 'accountController'
        }).state('editEvent', {
            url: '/events/edit/:id',
            templateUrl: 'pages/events/create',
            controller: 'eventsController'
        }).state('facebook', {
            url: '/events/link',
            templateUrl: 'pages/events/link',
            controller: 'linkController'
        })
}]);

/*
* ref : https://developers.facebook.com/docs/javascript/howto/angularjs
*/
mpApp.factory('facebookService', function($q){
    return {
        getPageById: function(pageId){
            var deferred = $q.defer();

            FB.api('/' + pageId + '?fields=cover{source},category,link,name,likes,events', function(response){
                deferred.resolve(response);
            });

            return deferred.promise;
        },
        searchPages: function(queryString){
            var deferred = $q.defer();
            // get pages by name search
            FB.api('/search?q=' + queryString + '&type=page&fields=id,name,likes,category', function(response){
                deferred.resolve(response.data)
            });

            return deferred.promise;
        },
        /*
        * Function that gets the connected user's events.
        *
        * Not in use since it requires the user_events permission to be reviewed
        * by facebook.
        */
        getLoggedInUserEvents: function(){
            var deferred = $q.defer();
            var getEvents = function(){
                // testing : permissions trying to get user_events permission...
                FB.api('/me/events', function(response){
                    if (!response || response.error){
                        deferred.reject('Error occured while trying to access events');
                    } else {
                        deferred.resolve(response.data);
                    }
                });
            }


            FB.getLoginStatus(function(response){
                // verify authentication with facebook
                if (response.status !== 'connected' && response.status !== 'not_authorized'){
                    FB.login(function(response){
                        if (response.authResponse){
                            alert('not logged in');
                            getEvents();
                        }
                    }, {scope: 'public_profile,email,user_events'});
                } else {
                    // verify or request user_events permission
                    FB.api('/me/permissions', function(response){
                        var permissions = response.data;
                        var eventsPermissionsGranted = false;

                        permissions.forEach(function(permission){
                            if (permission['permission'] == 'user_events'){
                                if (permission['status'] == 'granted'){
                                    eventsPermissionsGranted = true;
                                    alert('TESTING, eventsController line 941: events permission granted');
                                }
                            }
                        });

                        if (!eventsPermissionsGranted){
                            FB.login(function(response){
                                if (response.authResponse){
                                    FB.api('/me/permissions', function(response){
                                        var eventsPermissionsGranted = false;

                                        response.data.forEach(function(permission){

                                            if (permission['permission'] == 'user_events' && permission['status'] == 'granted'){
                                                eventsPermissionsGranted = true;
                                                getEvents();
                                            }
                                        });

                                        if (!eventsPermissionsGranted){
                                            alert("Impossible to parse request :\n\nPlease grant MuCity access to your Facebook events.");
                                        }
                                    });
                                }
                            },{scope: 'user_events', auth_type: 'rerequest'})
                        } else {
                            getEvents();
                        }
                    });
                }
            });

            return deferred.promise;
        }
    }
});
