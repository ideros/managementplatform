/**
 * Created by Ilias on 2/25/2015.
 * Source : https://developers.facebook.com/docs/facebook-login/login-flow-for-web/v2.2
 */
// This is called with the results from from FB.getLoginStatus().
function statusChangeCallback(response) {
  // The response object is returned with a status field that lets the
  // app know the current login status of the person.
  // Full docs on the response object can be found in the documentation
  // for FB.getLoginStatus().
  if (response.status === 'connected') {
    // Logged into your app and Facebook.
    $('#token').val(response.authResponse.accessToken);
    $('#facebook').submit();
  } else if (response.status === 'not_authorized') {
    // The person is logged into Facebook, but not your app.
    $('#error').html('Please log ' +
    'into this app.');
    $('.alert', $('#login')).show();
  } else {
    // The person is not logged into Facebook, so we're not sure if
    // they are logged into this app or not.
      $('#error').html('Not logged into facebook.');
      $('.alert', $('#login')).show();
  }
}

// This function is called when someone finishes with the Login
// Button.  See the onlogin handler attached to it in the sample
// code below.
function checkLoginState() {
  FB.getLoginStatus(function(response) {
    statusChangeCallback(response);
  });
}
