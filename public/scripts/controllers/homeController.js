/**
 * Created by Ilias on 2/25/2015.
 */
'use strict'

mpApp.controller('homeController', ['$scope', '$http', '$rootScope', function($scope, $http, $rootScope){
    $rootScope.title = 'Home';

    $http.get('/api/user').success(function (userData){
        $scope.welcomeMsg = 'Welcome home, ' + (userData.DisplayName || userData.Username) + ' !';
    }).error(function(error){
        $scope.welcomeMsg = 'Error, unable to retrieve users display name.';
    });
}]);