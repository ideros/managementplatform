'use strict'

mpApp.controller('accountController', ['$scope', '$http', '$rootScope', function($scope, $http, $rootScope){
    $rootScope.title = 'My account';
    
    $scope.newArtist = {};

    $http.get('/api/user').success(function (userData){
        $scope.user = userData;

        if ($scope.user.Artist){
            showBrowseArtistMessage("Fetching currently linked artist info...");

            $http.get('/api/artist/' + $scope.user.Artist).success(function(body){
                $scope.user.Artist = JSON.parse(body).Result[0];
                $scope.artistProfileUpdateStatus = "";
            }).error(function(error){
                $scope.artistProfileUpdateClass = "red-text";
                $scope.artistProfileUpdateStatus = 'An unexpected server error occurred while getting artist info.';
            });
        }
        if (!$scope.user.Picture){
            $scope.user.Picture = "/public/img/account-default.png";
        }
    });

    $scope.changePassword = function(account){
        $scope.passwordError = '';
        $scope.passwordSuccess = '';

        if (!$scope.currentPassword || $scope.currentPassword == ''){
            $scope.currentPassword = '';
            $scope.currentPasswordError = 'Enter your current password';
        }

        if (!$scope.password || $scope.password == ''){
            $scope.password = '';
            $scope.confirmPassword = '';
            $scope.userPasswordError = 'Enter your new password';
        } else if ($scope.confirmPassword != $scope.password){
            $scope.password = '';
            $scope.confirmPassword = '';
            $scope.confirmPasswordError = "Password and confirmation must be identical";
        }

        // password is validated
        if ($scope.password != '' && $scope.currentPassword != ''){
            $http.post('/api/account/changePassword', {'username' : $scope.user.Username, 'currentPassword' : $scope.currentPassword,'password' : $scope.password })
                .success(function(data, status, headers, config){
                    $scope.currentPassword = '';
                    $scope.password = '';
                    $scope.confirmPassword = '';

                    if (data.message && ~data.message.indexOf("password")){
                        $scope.currentPasswordError = "Incorrect password.";
                    } else {
                        $scope.passwordSuccess = "Password successfully updated !";
                    }
                })
                .error(function(data, status, headers, config){
                    $scope.passwordError = "An unexpected error occured while updating password. Please try again later.";
                });
        }
    }

    $scope.saveAccountEdit = function(account){
        $scope.updateStatus = 'updating';

        $http.post('/api/account/update', {'accountData' : account})
        .success(function(data, status, headers, config){
            if (data.Status == 'success'){
                $scope.updateStatus = 'updated';
            } else {
                $scope.updateStatus = 'error';
            }
        })
        .error(function(data, status, headers, config){
            $scope.updateStatus = 'error';
        });
    }
    
    $scope.createArtist = function(artist){
        $scope.createArtistStatus = 'uploading';
        $http.post('/api/artist/create', {'artistData' : artist})
            .success(function(data, status, headers, config){
                if (data.Status == 'success'){                    
                    $scope.newArtist = {};  // clear data
                    $scope.fileread = null;
                    $scope.showForm = 'info';   // hide "create artist" form
                    $scope.createArtistStatus = 'success';
                    alert('Artist "' + artist.Name + '" successfully created !');                    
                } else {
                    $scope.createArtistStatus = 'createError';
                    alert("Error creating artist");              
                }
            })
            .error(function(){
                $scope.createArtistStatus = 'httpError';
                alert("http post error");
            });
    }
    var CLOUDINARY_LINK = "http://res.cloudinary.com/appolomusic/image/upload/v1429453866/";
    $scope.uploadCover = function(uri){
        $scope.coverUploadStatus = 'uploading';
        $http.post("/api/upload", { "url" : uri, "eventId" : $scope.event.Id }).
            success(function(response){
                $scope.event.Cover = CLOUDINARY_LINK + response.id;
                $scope.coverUploadStatus = 'uploaded';
            }).
            error(function(response){
                $scope.coverUploadStatus = 'error';
            }
        );
    }

    var displayedArtistsIds = [];
    $scope.browseArtists = function(searchText){
        var displayList = [];
        displayedArtistsIds = [];

        if (searchText && searchText.length > 2){
            showBrowseArtistMessage("Searching artists names containing '" + searchText + "'");
            $http.get('/api/artistsNamesList/search=' + searchText).success(function (artists) {
                artists = JSON.parse(artists);

                if (artists.Count == 0){
                    showBrowseArtistMessage("No artists found with name containing '" + searchText + "'.");
                } else {
                    showBrowseArtistMessage("");
                    artists.Result.forEach(function(artist){
                        displayList.push(artist.Name);
                        displayedArtistsIds.push(artist.Id);
                    });
                }
            });
        }

        $scope.artists = displayList;
    }

    $scope.selectArtist = function(selectedIndex){
        var artistId = displayedArtistsIds[selectedIndex];
        showBrowseArtistMessage("Loading artist info...");

        if ($scope.user.Artist && $scope.user.Artist.Id == artistId){
            $scope.artistProfileUpdateClass = "green-text";
            $scope.artistProfileUpdateStatus = 'Your account is already linked to "' + $scope.user.Artist.Name + '" !';
        } else {
            $http.get('/api/artist/' + artistId).success(function(response){
                var artist = JSON.parse(response).Result[0];
                $scope.user.Artist = artist;

                if (!$scope.user.Artist.SoundCloudPage || $scope.user.Artist.SoundCloudPage == "aa"){
                    $scope.user.Artist.SoundCloudPage = $scope.user.Artist.SoundcouldPage;
                }
                showBrowseArtistMessage("Linking artist profile to your account...");

                $http.get('/api/artist/userlink/' + artistId).success(function(response){
                    var usersLinkedWithArtistCount = JSON.parse(response).Count

                    if (!usersLinkedWithArtistCount){
                        $http.post('/api/user/artist', {'id' : $scope.user.Id, 'artist' : artistId}).success(function(response){
                            if (response.status == 'success'){
                                $scope.artistProfileUpdateClass = "green-text";
                                $scope.artistProfileUpdateStatus = '"' + artist.Name + '" successfully linked to your account !';
                            } else {
                                $scope.artistProfileUpdateClass = "red-text";
                                $scope.artistProfileUpdateStatus = 'An unexpected server error occurred while updating your profile.';
                            }
                        }).error(function(error){
                            $scope.artistProfileUpdateClass = "red-text";
                            $scope.artistProfileUpdateStatus = 'An unexpected server error occured.';
                        });
                    } else {
                        $scope.artistProfileUpdateClass = "red-text";
                        $scope.artistProfileUpdateStatus = 'Another user is currently linked to "' + artist.Name + '".';
                    }
                });
            });
        }
    }

    function showBrowseArtistMessage(message){
        $scope.artistProfileUpdateClass = "grey-text";
        $scope.artistProfileUpdateStatus = message;
    }
}]);

mpApp.directive('tabs', function(){
    return {
        restrict: 'C',
        link: function(scope, elem){
            elem.tabs();
        }
    }
});

mpApp.directive('datepicker', function(){
    return {
        restrict: 'C',
        link: function(scope, elem){
            elem.pickadate({
                selectMonths: true,
                selectYears: 90
            });
        }
    }
})
