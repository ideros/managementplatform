/**
 * Created by Ilias on 2/25/2015.
 */
'use strict'

mpApp.controller('linkController', ['$scope', '$http', '$rootScope', 'facebookService',
    function($scope, $http, $rootScope, facebookService){
    var MIN_SEARCH_LENGTH = 5;
    var MAX_QUERY_PAGES = 25;   // maximum amount of pages contained in facebook response when querying events
    $rootScope.title = 'Link with Facebook';
    var userId;
    $scope.syncList = [];
    $scope.unsyncList = [];
    $scope.pages = [];
    $scope.pagesLoading = 'loading';

    $http.get('/api/user').success(function(user){
        userId = user.Id;

        $http.get('/api/pages/' + userId).success(function(response){
            if (response.Status == 'success'){
                var pages = response.pages;
                // get page details from ID
                for (var i = pages.length;i-- > 0;){
                    var pageSyncStatus = pages[i].Sync;

                    facebookService.getPageById(pages[i].FBID)
                        .then(function(page){
                            if (page && page.name){
                                page.Sync = pageSyncStatus;
                                $scope.pages.push(page);
                                $scope.pagesLoading = 'loaded';

                                // get only event Ids
                                if (page.events){
                                    var eventIds = new Array(page.events.data.length);
                                    for (var i = 0;i < eventIds.length;i++){
                                        eventIds[i] = page.events.data[i].id;
                                    }

                                    // find which event are Synced
                                    $http.post("/api/events/filterSynced", {'ids' : eventIds})
                                        .success(function(response){
                                            // set Sync to true
                                            response.forEach(function(syncedEventId){
                                                page.events.data[eventIds.indexOf(syncedEventId.FBID)].Sync = true;
                                            });
                                        });
                                }
                            } else {
                                // Occurs when facebook returns "OAuth" error
                                $scope.pagesLoading = 'error';
                            }
                        });
                }

                if (pages.length == 0){
                    $scope.pagesLoading = 'loaded';
                }
            }
        }).error(function(error){
            $scope.pagesLoading = 'error';
        });
    }).error(function(error){
        $scope.pagesLoading = 'error';
    });

    var displayList = [];
    var displayedPagesIds = [];
    $scope.selectPageTooltip = "Type in box below to search through Facebook events";

    $scope.unsyncPageAndEvents = function(pageIndex){
        var pageToUnsync = $scope.pages[pageIndex];
        var ids = [];

        pageToUnsync.events && pageToUnsync.events.data.forEach(function(event){
            ids.push(event.id);
        });

        $http.post('/api/page/unmanage/' + pageToUnsync.id, {"eventFBIDs" : ids})
            .success(function(data){
                $scope.pages.splice(pageIndex, 1);
            }).error(function(error){
                alert("An unexpected error occured while unlinking page. Please try again later.")
            });
    }

    $scope.managePage = function(page){
        $http.post('/api/page/manage/' + page.id, {'userId' : userId})
          .success(function(data){
              if (data.Status == 'success'){
                  $scope.tooltipClass = 'success';

                  if (data.page == 'existing'){
                      if (data.manager){
                          if (data.manager == 'self'){
                              $scope.selectPageTooltip = "You are already managing \"" + page.name + "\" !";
                          } else {
                              $scope.tooltipClass = 'error';
                              $scope.selectPageTooltip = "Someone is already managing this page. Only one person may edit a page's details using this platform.";
                          }
                      } else {
                          $scope.pages.push(page);
                          $scope.selectPageTooltip = "You may now sync \"" + page.name + "\"'s events with MuCity.";
                      }
                  } else {
                      $scope.pages.push(page);
                      $scope.selectPageTooltip = "Page \"" + page.name + "\" added to page list. You may now sync it's events with MuCity.";
                  }
              } else {
                  $scope.tooltipClass = 'error';
                  $scope.selectPageTooltip = "Unable to access page \"" + page.name + "\".";
              }
        }).error(function(error){
            $scope.tooltipClass = 'error';
            $scope.selectPageTooltip = "Error syncing page. Please try again later";
        });
    }

    $scope.selectPage = function(selectedIndex){
        var pageId = displayedPagesIds[selectedIndex];
        $scope.tooltipClass = 'info';
        $scope.selectPageTooltip = "Syncing page...";

        facebookService.getPageById(pageId)
            .then(function(page){
                if (page){
                    $scope.managePage(page);
                } else {
                    $scope.tooltipClass = 'error';
                    $scope.selectPageTooltip = "Unable to retrieve page info.";
                }
            });
    }

    $scope.browsePages = function(searchText){
        displayList = [];
        displayedPagesIds = [];

        if (searchText.length >= MIN_SEARCH_LENGTH){
            $scope.tooltipClass = 'info';
            $scope.selectPageTooltip = "Loading pages list...";

            facebookService.searchPages(searchText)
                .then(function(pages){
                    if (pages){
                        $scope.tooltipClass = 'info';
                        if (pages.length > 0){
                            pages = sortResults(pages, 'likes', false);

                            pages.forEach(function(page){
                                displayList.push(page.name + ', ' + page.category + '  |  ' + page.likes + ' likes');
                                displayedPagesIds.push(page.id);
                            });

                            if (displayList.length == pages.length){
                                if (displayList.length >= MAX_QUERY_PAGES){
                                    $scope.tooltipClass = 'error';
                                    $scope.selectPageTooltip = "Over " + MAX_QUERY_PAGES + " events containing \"" + searchText + "\". Please make a more specific search.";
                                } else {
                                    $scope.selectPageTooltip = "Select a page from which you would like to sync events";
                                }
                            }
                        } else {
                            $scope.selectPageTooltip = "No page found containing \"" + searchText + "\"";
                        }
                    } else {
                        $scope.tooltipClass = 'error';
                        $scope.selectPageTooltip = "Unable to access Facebook pages."
                    }
                });
        } else {
            $scope.selectPageTooltip = "Type " + (MIN_SEARCH_LENGTH - searchText.length)
            + ' more ' + (searchText.length > 1 ? 'character' : 'characters');
            $scope.tooltipClass = 'info';
        }

        $scope.pagesBrowse = displayList;
    }

    $scope.formatTime = function(timestamp){
        var date = new Date(timestamp);

        return (date.getDate() + "/" + (date.getMonth() + 1) + "/" + date.getFullYear());
    }

    $scope.syncPage = function(page, syncStatus){
        var pageId = page.id;

        if (syncStatus){
            if (~$scope.unsyncList.indexOf(pageId)){
                $scope.unsyncList.splice($scope.unsyncList.indexOf(pageId), 1);
            } else {
                $scope.syncList.push(pageId);
            }

            page.events.data.forEach(function(event){
                $scope.syncEvent(event.id, false);
            });
        } else {
            if (~$scope.syncList.indexOf(pageId)){
                $scope.syncList.splice($scope.syncList.indexOf(pageId), 1);
            } else {
                $scope.unsyncList.push(pageId);
            }

            page.events.data.forEach(function(event){
                if (event.Sync){
                    $scope.syncEvent(event.id, true);
                }
            });
        }
    }

    $scope.syncEvent = function(eventId, syncStatus){
        if (syncStatus){
            if (~$scope.unsyncList.indexOf(eventId)){
                $scope.unsyncList.splice($scope.unsyncList.indexOf(eventId), 1);
            } else {
                $scope.syncList.push(eventId);
            }
        } else {
            if (~$scope.syncList.indexOf(eventId)){
                $scope.syncList.splice($scope.syncList.indexOf(eventId), 1);
            } else {
                $scope.unsyncList.push(eventId);
            }
        }
    }

    $rootScope.$on('$locationChangeStart', function(next, current) {
        if ($scope.syncList.length > 0 || $scope.unsyncList.length > 0){
            $http.post('/api/pages/sync', {'userId' : userId, 'ids' : $scope.syncList});
            $http.post('/api/pages/unsync', {'userId' : userId, 'ids' : $scope.unsyncList});
            alert("synced : " + $scope.syncList + "\nunsynced : " + $scope.unsyncList);

            $scope.syncList = [];
            $scope.unsyncList = [];
        }
     });
}]);

function sortResults(json, prop, asc) {
    json = json.sort(function(a, b) {
        if (asc) return (a[prop] > b[prop]) ? 1 : ((a[prop] < b[prop]) ? -1 : 0);
        else return (b[prop] > a[prop]) ? 1 : ((b[prop] < a[prop]) ? -1 : 0);
    });

    return json;
}

mpApp.directive('collapsible', function(){
    return {
        restrict: 'C',
        link: function(scope, elem){
            elem.on('mouseenter', function(){
                elem.collapsible();
            });
        }
    }
})
