/**
 * Strictly used inside the login page. Does not bridge between login
 * and panel index.
 *
 * Created by Ilias on 2/25/2015.
 */
'use strict';

mpApp.controller('authenticationController', ['$scope', '$http', function($scope, $http){
    $scope.registerUser = function(user){

        if (!user.email || user.email.length == 0){
            $scope.registerError = "Please input an Email address for your account.";
        } else if(!(/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i).test(user.email)){    // if email is valid
            $scope.registerError = "Invalid Email address.";
        } else if (!user.password || user.password.length == 0){
            $scope.registerError = "Please enter a password for your account.";
        } else if(!user.confirmPassword || user.confirmPassword.length == 0){
            $scope.registerError = "Please confirm your password.";
        } else if (user.password != user.confirmPassword){
            $scope.registerError = "Your password confirmation must be the same as the password.";
        } else {
            $scope.registerError = "";
            $scope.register(user);
        }
    }

  $scope.register = function(regData){
    $http.post('register', { 'email' : regData.email, 'password' : regData.password }).
      success(function(data, status, headers, config){
          data = data.data;

          if (data){
              if (data.result && data.result.Id){
                  $scope.showForm = 'login';
                $scope.loginSuccessMsg = 'Registration successful. You may now sign in using "' + regData.email + '" !';
                $scope.registerError = '';
                regData.email = '';
              } else if (data.message){
                $scope.registerError = data.message.replace("username", "email");   // email is provided as username to make use of backend verification
              } else {
                $scope.registerError = 'An unexpected error has occured while processing your request. Please try again.';
              }
          } else {
              $scope.registerError = 'An unexpected error has occured while processing your request. Please try again.';
          }

        regData.password = '';
        regData.confirmPassword = '';
      }).
      error(function(data, status, headers, config){
        $scope.registerError = 'A network error prevented your request. Please check your internet connection and try again.';

        regData.password = '';
        regData.rpassword = '';
      });
  };

  $scope.recoverPassword = function(emailInput){
      if (emailInput){
          if ((/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i).test(emailInput)){

              $http.post('https://api.everlive.com/v1/Xi7yEjFuv1A5vbBO/Users/resetpassword'
                , {'Username': emailInput}).
                success(function(data, status, headers, config){
                    $scope.showForm = 'login';
                    $scope.recoverError = '';
                    $scope.recoveryEmail = '';
                  $scope.loginSuccessMsg = 'A password recovery email has been sent to "' + emailInput + '".';
                }).
                error(function(data, status, headers, config){
                   $scope.recoverError = data.message.replace("username", "email");   // email is provided as username to make use of backend verification
                });
            } else {
                $scope.recoverError = "Invalid email format. (valid@example.com)";
            }
      } else {
          $scope.recoverError = "Please type in your account's Email address before submiting";
      }
  };
}]);
