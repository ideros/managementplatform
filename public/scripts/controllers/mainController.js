/**
 * Controller for the template parts that are always
 * present.
 *
 * Created by Ilias on 2/25/2015.
 */
'use strict';

mpApp.controller('mainController', ['$scope', '$rootScope', function($scope, $rootScope){
    $rootScope.$on('$stateChangeStart', function(event, toState){
       $rootScope.state = toState.name;
    });
    
    $scope.eventMode = false;

    $scope.setEventMode = function(isEventMode){
        $scope.eventMode = isEventMode;
    }
}]);
