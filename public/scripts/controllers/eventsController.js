/**
 * Created by Ilias on 2/25/2015.
 */
'use strict'

mpApp.controller('eventsController', ['$scope', '$rootScope', '$http', '$stateParams', '$location', 'facebookService', 'Upload',
    function ($scope, $rootScope, $http, $stateParams, $location, facebookService, fileUploader){
        // Mixtracks
        var MAXIMUM_MIXTRACKS_COUNT = 5;
        $scope.mixtrack = {};
        $scope.uploadedMixtracks = [];
        $scope.eventArtists = [];
        $scope.suggestedArtists = [];

        $scope.uploadMixtrack = uploadMixtrack;
        $scope.selectMixtrackArtist = selectMixtrackArtist;
        $scope.browseEventArtists = browseEventArtists;

        // The rest
        var DEFAULT_COVER_LINK = "http://res.cloudinary.com/appolomusic/image/upload/v1429453866/szlhvseugkc38i8q64of";

        // BEGIN ARTISTS
        $scope.selectArtistTooltip = "";
        /*
        * @searchText contains at least three characters.
        * @returns list of artist names and their IDs (sets "artists" data)
        */
        var MIN_SEARCH_LENGTH = 3;
        var displayedArtistsIds = [];

        $scope.getLoggedInUserEvents = function(){
            facebookService.getLoggedInUserEvents()
                .then(function(response){
                    alert('Permissions : ' + JSON.stringify(response));
                });
        };

        $scope.showRemainingStepsWarning = function(){
            Materialize.toast('You must complete all required steps !', 2000);
        }

        $scope.StepEnum = {
            TIME : 0,
            DETAILS : 1,
            COVER : 2,
            PHOTOS : 3,
            ARTISTS : 4,
            LOCATION : 5,
            SOCIAL : 6,
            MIXTRACK: 7
        };

        var Sources = {
            EVERLIVE : 'everlive',
            CLOUDINARY : 'cloudinary'
        }

        /**
        * Link before ID
        */
        var CLOUDINARY_LINK = "http://res.cloudinary.com/appolomusic/image/upload/v1429453866/";

        $scope.nextStep = function(){
            ++$scope.step;
            // skip summary sections when browsing through sequentially or go back to overview if editing a specific section
            if ($scope.step === $scope.editElement){

                if ($scope.editSection || $scope.editElement){
                    // go back to section that was being edited
                    do{
                        $scope.setStep(--$scope.step);
                    }while(!($scope.step === $scope.StepEnum.TIME || $scope.step === $scope.StepEnum.DETAILS))
                } else {
                    $scope.setStep(++$scope.step);
                }
            } else {
                $scope.setStep($scope.step);
            }
        }

        $scope.setStep = function(step){
            $scope.step = step;

            var clearEditState = function(){
                $scope.editSection = false;
                $scope.editElement = false;
            }

            switch(step){
                case $scope.StepEnum.TIME:
                    $scope.title = 'When';
                    clearEditState();
                    break;
                case $scope.StepEnum.DETAILS:
                    $scope.title = 'Presentation';
                    clearEditState();
                    break;
                case $scope.StepEnum.COVER:
                    $scope.title = 'Cover';
                    $scope.getEventPictures();
                    clearEditState();
                    break;
                case $scope.StepEnum.PHOTOS:
                    $scope.title = 'Photos';
                    $scope.getEventPictures();
                    clearEditState();
                    break;
                case $scope.StepEnum.ARTISTS:
                    $scope.title = 'Artists';
                    clearEditState();
                    break;
                case $scope.StepEnum.LOCATION:
                    $scope.title = 'Location';
                    clearEditState();
                    break;
                case $scope.StepEnum.SOCIAL:
                    $scope.title = 'Links';
                    clearEditState();
                    break;
            }
        }
        /**
        * @param : date DATE to format
        * @return: STRING in the following format : 2014-11-01T23:30:00.000Z
        */
        function formatForDB(date){
            var year, month, day, hours, minutes, seconds;

            year = date.getFullYear();
            month = (date.getMonth() + 1) > 9 ? (date.getMonth() + 1) : "0" + (date.getMonth() + 1);    // +1 because month is saved from 1 to 12
            day = date.getDate() > 9 ? date.getDate() : "0" + date.getDate();
            hours = date.getHours() > 9 ? date.getHours() : "0" + date.getHours();
            minutes = date.getMinutes() > 9 ? date.getMinutes() : "0" + date.getMinutes();
            seconds = date.getSeconds() > 9 ? date.getSeconds() : "0" + date.getSeconds();

            return year + "-" + month + "-" + day + "T" + hours + ":" + minutes + ":" + seconds + ".000Z";   // always 0 milliseconds
        }

        var MONTH_TEXT = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
        var eventId = $stateParams.id;  // GET EVENT ID (undefined on event creation)


        if (eventId) {
            $rootScope.title = "Edit event";
            $scope.eventLoad = 'loading';

            $http.get('/api/event/' + eventId).success(function(response){
                $scope.event = JSON.parse(response).Result[0];
                "Y\/m\/d\,\ H\:i";
                $scope.checkIfEndIsEarlierThanBegin($scope.event.Date, $scope.event.End);

                var date = new Date($scope.event.Date);
                var end = new Date($scope.event.End);

                $scope.event.Date = timestampToDate($scope.event.Date);
                $scope.event.End = timestampToDate($scope.event.End);
                if ($scope.event.Venue){
                    $scope.event.Venue.CoverLink = CLOUDINARY_LINK + $scope.event.Venue.CoverLink;
                } else {
                    $scope.event.Venue = {
                        'Name' : $scope.event.Place,
                        'Address' : $scope.event.Address
                    }
                }
                $scope.eventLoad = 'loaded';

                for (var i = $scope.event.Artists.length;i-- > 0;){
                    $scope.eventArtists.push($scope.event.Artists[i].Name);
                }
            }).error(function(error){
                console.log('an error occured while accessing the event you wanted to edit. It might have been removed.');
            });
        } else {
            $rootScope.title = 'My Events';

            $scope.event = {};
            $scope.event.Artists = [];
            $scope.event.Photos = [];

            $scope.eventLoad = 'loaded';
        }

    // BEGIN ARTISTS
    $scope.selectArtistTooltip = "";
    /*
    * @searchText contains at least three characters.
    * @returns list of artist names and their IDs (sets "artists" data)
    */
    var MIN_SEARCH_LENGTH = 3;
    var displayedArtistsIds = [];
    $scope.browseArtists = function(searchText){
        var displayList = [];
        displayedArtistsIds = [];
        $scope.showUndo = false;

        if (searchText.length >= MIN_SEARCH_LENGTH){
            $scope.tooltipClass = "info";
            $scope.selectArtistTooltip = "Loading artists list...";

            $http.get('/api/artistsNamesList/search=' + searchText).success(function (artists) {
                artists = JSON.parse(artists).Result;

                artists.forEach(function(artist){
                    displayList.push(artist.Name);
                    displayedArtistsIds.push(artist.Id);
                });

                if (displayList.length > 0){
                    $scope.tooltipClass = "info";
                    $scope.selectArtistTooltip = "Select an artist to add to line-up";
                } else {
                    $scope.tooltipClass = "info";
                    $scope.selectArtistTooltip = "No name found containing \"" + searchText + "\"";
                }
            }).error(function (data) {
                $scope.tooltipClass = "error";
                $scope.selectArtistTooltip = "Unable to retrieve artists list";
            });
        } else {
            var remainingChars = MIN_SEARCH_LENGTH - searchText.length;

            $scope.selectArtistTooltip = "Type in " + remainingChars + " more "
                + (remainingChars > 1 ? "characters" : "character");
        }

        $scope.artists = displayList;
    }

    $scope.selectArtist = function(selectedIndex){
        var artistId = displayedArtistsIds[selectedIndex];

        artistIsAlreadyInLineup(artistId, $scope.event.Artists
            , function(){
            $scope.tooltipClass = "info";
            $scope.selectArtistTooltip = "Adding artist to line-up...";

            $http.get('/api/artist/' + artistId).success(function(response){
                var artist = JSON.parse(response).Result[0];

                $scope.event.Artists.push(artist);
                $scope.tooltipClass = "success";
                $scope.selectArtistTooltip = "Artist added to line-up !";
            }).error(function(error){
                $scope.tooltipClass = "error";
                $scope.selectArtistTooltip = "Unable to add artist to event";
            });
        }, function(){
            $scope.tooltipClass = "info";
            $scope.selectArtistTooltip = "This artist is already part \
            of the line-up !";
        });
    }

    $scope.removeArtistFromLineup = function(removedArtist){
        var idIndex = $scope.event.Artists.indexOf(removedArtist);

        $scope.event.Artists.splice(idIndex, 1);

        $scope.tooltipClass = "info";
        $scope.selectArtistTooltip = 'Artist "' + removedArtist.Name
        + '" removed from line-up';

        $scope.showUndo = true;
        $scope.undoAction = function(){
            $scope.tooltipClass = "success";
            $scope.selectArtistTooltip = 'Artist "' + removedArtist.Name + '" restaured in line-up';
            $scope.showUndo = false;

            $scope.event.Artists.splice(idIndex, 0, removedArtist);
        }
    }
    // END ARTISTS
    // BEGIN PHOTOS
    $scope.MAX_PHOTOS = 30;
    $scope.photoCount = 0;
    $scope.getEventPictures = function(){
        if (eventId){
            if (!$scope.imagesLoaded){
                $scope.loadPhotoStatus = 'loading';
                $http.get('/api/event/images/' + eventId).success(function(response){
                    var event = angular.fromJson(response).Result;
                    // event will be undefined if it has no everlive images
                    if (event){
                        $scope.event.Image = event[0].Image;    // Cover picture
                        $scope.event.Photos = [];
                        // add everlive photos
                        if (event[0].Photos) {
                            for(var i = event[0].Photos.length;i-- > 0;){
                                var photo = {};
                                photo.Url = event[0].Photos[i].Uri;
                                photo.Id = event[0].Photos.Id;
                                photo.Source = Sources.EVERLIVE;

                                $scope.event.Photos.push(photo);
                            }
                        }
                    } else {
                        $scope.event.Image = [];
                        $scope.event.Photos = [];
                    }

                    // add cloudinary photos
                    if ($scope.event.Photos_Url){

                        for(var i = $scope.event.Photos_Url.length;i-- > 0;){
                            var photo = {};
                            photo.Url = $scope.event.Photos_Url[i];
                            photo.Id = photo.Url.substring(photo.Url.lastIndexOf('/') + 1);
                            // remove image extension if any
                            if (!!~photo.Id.indexOf('.')){
                                photo.Id = photo.Id.substring(0, photo.Id.lastIndexOf('.'));
                            }
                            photo.Source = Sources.CLOUDINARY;

                            $scope.event.Photos.push(photo);
                        }
                    }

                    $scope.photoCount = $scope.event.Photos.length;
                    $scope.imagesLoaded = true; // load images only once
                    $scope.loadPhotoStatus = 'ready';
                }).error(function(error){
                    $scope.loadPhotoStatus = 'error';
                });
            }
        } else {
            $scope.loadPhotoStatus = 'ready';
            $scope.event.Cover = DEFAULT_COVER_LINK;
        }
    }

    var uploadingEventPhotoIds = [];

    $scope.addPhoto = function($file){
        if ($file){
            var reader = new FileReader();
            reader.onload = function () {
                $scope.$apply(function () {
                    var photo = {};
                    photo.Url = loadEvent.target.result;

                    $scope.event.Photos.unshift(photo);
                    $scope.uploadSuccess = "Uploading image...";

                    $http.post("/api/upload", { "url" : photo.Url, "eventId" : $scope.event.Id }).
                        success(function(response){
                            $scope.uploadSuccess = "Image successfully uploaded !";
                            photo.Id = response.id;
                            photo.Added = true;
                            photo.Source = Sources.CLOUDINARY;
                            $scope.photoCount++;
                        }).
                        error(function(response){
                            $scope.uploadError = "Error uploading image. (Maximum file size : ~1MB)";
                            $scope.uploadSuccess = "";
                            $scope.event.Photos.splice(
                                $scope.event.Photos.indexOf(photo), 1);
                        }
                    );
                });
            }
            reader.readAsDataURL($file);
        } else {
            $scope.uploadError = "Error uploading image.";
        }
    };

    $scope.uploadCover = function(uri){
        $scope.coverUploadStatus = 'uploading';
        $http.post("/api/upload", { "url" : uri, "eventId" : $scope.event.Id }).
            success(function(response){
                $scope.event.Cover = CLOUDINARY_LINK + response.id;
                $scope.coverUploadStatus = 'uploaded';
            }).
            error(function(response){
                $scope.coverUploadStatus = 'error';
            }
        );
    }

    $scope.deletePhoto = function(index){
        $scope.event.Photos[index].Deleted = true;
        $scope.$apply(function(){
            $scope.photoCount--;
        });
    }

    $scope.restorePhoto = function(index){
        $scope.event.Photos[index].Deleted = false;
        $scope.$apply(function(){
            $scope.photoCount++;
        });
    }
    // END PHOTOS
    // BEGIN VENUE
    var displayedVenueIds = [];
    $scope.browseVenues = function(searchText){
        $scope.venues = [];
        displayedVenueIds = [];
        $scope.showUndo = false;

        if (searchText.length >= MIN_SEARCH_LENGTH){
            $scope.venueTooltipClass = "info";
            $scope.selectVenueTooltip = "Loading venues list...";

            $http.get('/api/venuesNamesList/search=' + searchText).success(function (venues) {
                venues = JSON.parse(venues).Result;

                venues.forEach(function(venue){
                    $scope.venues.push(venue.Name);
                    displayedVenueIds.push(venue.Id);
                });

                if ($scope.venues.length > 0){
                    $scope.venueTooltipClass = "info";
                    $scope.selectVenueTooltip = "Select the event venue";
                } else {
                    $scope.venueTooltipClass = "info";
                    $scope.selectVenueTooltip = "No name found containing \"" + searchText + "\"";
                }
            }).error(function (data) {
                $scope.venueTooltipClass = "error";
                $scope.selectVenueTooltip = "Unable to retrieve venues list";
            });
        } else {
            var remainingChars = MIN_SEARCH_LENGTH - searchText.length;

            $scope.selectVenueTooltip = "Type in " + remainingChars + " more "
                + (remainingChars > 1 ? "characters" : "character");
        }
    }

    $scope.selectVenue = function(selectedIndex){
        var venueId = displayedVenueIds[selectedIndex];

        $scope.venueTooltipClass = "info";
        $scope.selectVenueTooltip = "fetching venue info...";

        $http.get('/api/venue/' + venueId).success(function(response){
            var venue = JSON.parse(response).Result[0];

            $scope.event.Venue = venue;
            // Coverlink only has the id of the cloudinary image.
            $scope.event.Venue.CoverLink = CLOUDINARY_LINK + venue.CoverLink;
            $scope.venueTooltipClass = "success";
            $scope.selectVenueTooltip = "New venue set as event location !";
        }).error(function(error){
            $scope.venueTooltipClass = "error";
            $scope.selectVenueTooltip = "Unable to set this venue";
        });
    }
    // END VENUE
    // BEGIN TIME
    $scope.formatDateAddOffset = function(date){
        date = new Date(date);
        var minutes = date.getMinutes();
        var tzDifferenceMilliseconds = date.getTimezoneOffset() * 60 * 1000;

        date = new Date(date.getTime() + tzDifferenceMilliseconds);

        return MONTH_TEXT[date.getMonth()] + " " + date.getDate() + ", " + date.getFullYear()
         + " at " + date.getHours() + "h" + (minutes >= 10 ? minutes : "0" + minutes);
    }

    $scope.keepSameIntervalWithEndDate = function(newDate) {
        if ($scope.event.End){
            var yearDif, monthDif, dateDif, hoursDif, minutesDif;
            var beginDate = new Date($scope.event.Date);
            var endDate = new Date($scope.event.End);

            yearDif = endDate.getFullYear() - beginDate.getFullYear();
            monthDif = endDate.getMonth() - beginDate.getMonth();
            dateDif = endDate.getDate() - beginDate.getDate();
            hoursDif = endDate.getHours() - beginDate.getHours();
            minutesDif = endDate.getMinutes() - beginDate.getMinutes();

            endDate.setFullYear(newDate.getFullYear() + Math.abs(yearDif));
            endDate.setMonth(newDate.getMonth() + Math.abs(monthDif));
            endDate.setDate(newDate.getDate() + Math.abs(dateDif));
            endDate.setHours(newDate.getHours() + Math.abs(hoursDif));
            endDate.setMinutes(newDate.getMinutes() + Math.abs(minutesDif));

            $scope.event.End = endDate;
        }
    };

    $scope.checkIfEndIsEarlierThanBegin = function(dateBegin, dateEnd){
        dateBegin = new Date(dateBegin);
        dateEnd = new Date(dateEnd);

        $scope.endDateBeforeBegin = dateEnd < dateBegin;
    };
    // END TIME

    $scope.update = function(event){
        // set only artists' ids in artists array
        for (var i = event.Artists.length;i-- > 0;){
            event.Artists[i] = event.Artists[i].Id;
        }

        // add photos if there are any
        var cloudinaryImageIdsToDelete = [], everliveImageIdsToDelete = [];
        if (event.Photos){
            if (!event.Photos_Url){
                event.Photos_Url = [];
            }

            for (var i = event.Photos.length;i-- > 0;){
                // remove photo
                if (event.Photos[i].Deleted){
                    switch(event.Photos[i].Source){
                        case Sources.CLOUDINARY:
                            cloudinaryImageIdsToDelete.push(event.Photos[i].Id);
                            var photoUrl = CLOUDINARY_LINK + event.Photos[i].Id;
                            var indexUrl = event.Photos_Url.indexOf(photoUrl);

                            if (!!~indexUrl){
                                event.Photos_Url.splice(indexUrl, 1);
                            }
                            break;
                        case Sources.EVERLIVE:
                            // FIXME: Error updating an event's legacy(everlive) photos
                            // everliveImageIdsToDelete.push(event.Photos[i].Id);
                            break;
                    }
                }
                else
                // added photos now upload strictly to CLOUDINARY
                if (event.Photos[i].Added){
                    var photoUrl = CLOUDINARY_LINK + event.Photos[i].Id;
                    event.Photos_Url.push(photoUrl);
                }
            }
        }

        $http.post('/api/delete/images', {"cloudinaryImageIds" : cloudinaryImageIdsToDelete,
            "everliveImageIds" : everliveImageIdsToDelete});

        event.Date = dateToTimestamp(event.Date);
        event.End = dateToTimestamp(event.End);

        event.Place = event.Venue.Name;
        event.Address = event.Venue.Address;

        event.Venue = event.Venue.Id;

        $http.post('/api/event/' + event.Id, {'event' : event}).success(function(response){
            $location.path('events');

            if (response.Status == 'success'){
                Materialize.toast('Successfully updated "' + event.Name + '"', 4000);
            } else {
                Materialize.toast('Error updating "' + event.Name + '".', 4000);
            }
        }).error(function(error){
            $location.path('events');
           console.log('error posting the modified event : ' + JSON.stringify(error));
        });
    };

    $scope.create = function(event){
        // set only artists' ids in artists array
        for (var i = event.Artists.length;i-- > 0;){
            event.Artists[i] = event.Artists[i].Id;
        }

        // add photos if there are any
        event.Photos_Url = [];
        for (var i = event.Photos.length;i-- > 0;){
            if (!event.Photos[i].Deleted){
                var photoUrl = CLOUDINARY_LINK + event.Photos[i].Id;
                event.Photos_Url.push(photoUrl);
            }
        }

        if (!event.Cover){
            event.Cover = DEFAULT_COVER_LINK;
        }

        if (!event.Description){
            event.Description = '';
        }

        if (!event.Website){
            event.Website = '';
        }
        if (!event.Fbfeed){
            event.Fbfeed = '';
        }
        if (!event.Igfeed){
            event.Igfeed = '';
        }
        if (!event.Twfeed){
            event.Twfeed = '';
        }

        event.Date = dateToTimestamp(event.Date);
        event.End = dateToTimestamp(event.End);
        console.log('posting event : ' + JSON.stringify(event));
        $http.post('/api/events/create', {'event' : event}).success(function(response){
            $location.path('events');

            if (response.Status == 'success'){
                Materialize.toast('"' + event.Name + '" successfully created', 4000);
            } else {
                Materialize.toast('Error creating "' + event.Name + '".', 4000);
            }
        }).error(function(response){
            $location.path('events');
            console.log('error posting the modified event : ' + JSON.stringify(error));
        });
    };
    // format date string from "Y\/m\/d\,\ H\:i" to "Example: 2015-04-22T00:15:00.000Z"
    // also substract universal time offset
    function dateToTimestamp(date){
        var arrDate0Time1 = date.split(",");
        var arrDate = arrDate0Time1[0].split('/');  // [yyyy, mm, dd]
        var arrTime = arrDate0Time1[1].split(':');  // [hh, mm]
        arrTime[0] = arrTime[0].substring(1);   // remove blank space (, H:..)

        var timestamp = arrDate[0] + '-' + arrDate[1] + '-' + arrDate[2] + 'T'
        + arrTime[0] + ':' + arrTime[1] + ':' + '00.000Z';

        return timestamp;
    }

    // format date from string "Example: 2015-04-22T00:15:00.000Z" to "Y\/m\/d\,\ H\:i"
    function timestampToDate(timestamp){
        timestamp = new Date(timestamp);
        // add local offset
        var now = new Date();
        var tzDifferenceMilliseconds = now.getTimezoneOffset() * 60 * 1000;

        timestamp = new Date(timestamp.getTime() + tzDifferenceMilliseconds);

        return timestamp.getFullYear() + '/'
         + (timestamp.getMonth() >= 9 ? timestamp.getMonth() + 1 : ('0' + (timestamp.getMonth() + 1))) + '/'
         + (timestamp.getDate() > 9 ? timestamp.getDate() : ('0' + timestamp.getDate())) + ',\ '
         + (timestamp.getHours() > 9 ? timestamp.getHours() : '0' + timestamp.getHours()) + ':'
         + (timestamp.getMinutes() > 9 ? timestamp.getMinutes() : '0' + timestamp.getMinutes());
    }

    function uploadMixtrack(mixtrack){
        if (!mixtrack.files){
            alert('Please select a file to upload');
            return;
        }
        var file = mixtrack.files[0];
        var artist = $scope.selectedMixtrackArtist;
        var title = mixtrack.title;

        if (file && artist && title){
            if ($scope.uploadedMixtracks && $scope.uploadedMixtracks.length < MAXIMUM_MIXTRACKS_COUNT){
                var fileExtension = file.type.substring(file.type.indexOf('/') + 1);
                if (fileExtension === 'mp3'){
                    fileUploader.http({
                        url: '/api/mixtrack/upload',
                        data: {
                            artistId: artist.Id,
                            title: title,
                            file
                        }                    
                    }).progress(function(evt){
                        console.log('progress, evt : ' + JSON.stringify(evt));
                    }).success(function(data, status, headers, config){
                        alert('upload success, data : ' + JSON.stringify(data));
                        $scope.uploadedMixtracks.push(mixtrack);
                        
                        // clear inputs
                        $scope.selectedMixtrackArtist = "";
                        $scope.mixtrack = {};
                    }).error(function(){
                        alert('error uploading mixtrack.');
                    });
                } else {
                    alert("Unsupported filetype '." + fileExtension + "'. Only '.mp3' audio files are allowed.")
                }
            } else {
                alert("The limit of mixtrack uploads (" + MAXIMUM_MIXTRACKS_COUNT + ") has already been reached.")
            }
        } else {
            var error = "Prior to upload a mixtrack, please :\n";
            file || (error += '- select a file\n');
            artist || (error += '- select an artist\n');
            title || (error += '- Enter a title\n');

            alert(error);
        }
    }

    function selectMixtrackArtist(selectedIndex){
        $scope.event.Artists.forEach(function(artist){
           (artist.Name == $scope.suggestedArtists[selectedIndex]) 
           && ($scope.selectedMixtrackArtist = artist); 
        });
        
        $scope.suggestedArtists = [];
    }
    
    function browseEventArtists(searchText){
        $scope.suggestedArtists = [];
        
        if (searchText.length){
            angular.forEach($scope.eventArtists, function(artistName){
                ~artistName.toLowerCase().indexOf(searchText.toLowerCase()) && $scope.suggestedArtists.push(artistName);
            });
        }
    }

/* mdTable ********************************************************************/
  $scope.toggleSearch = false;

  $scope.headers = [
      {
          name: 'ID',
          field: 'Id'
      },
    {
      name:'',
      field:'thumb'
    },{
      name: 'Name',
      field: 'Name'
    },{
      name: 'Venue',
      field: 'Place'
    },{
      name: 'Date',
      field: 'Date'
    },{
      name: 'Description',
      field: 'Description'
    }
  ];
    $scope.content = [];
    $scope.loadingEvents = 'loading';
    $http.get('/api/user').success(function(data){
        var artistId = data.Artist;

        if (artistId){
            $http.get('/api/events/' + artistId).success(function(events){
                events = JSON.parse(events).Result;

                function getMonth(date){
                    var longMonths = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']
                    return longMonths[date.getMonth()];
                };

                for(var i = 0;i < events.length;i++){
                    var event = events[i];
                    var _thumb;

                    if (event.Cover){
                        _thumb = event.Cover;
                    } else {
                        _thumb = "/public/img/default-cover.png"; // default venue image
                    }

                   var eventPreview = {
                       thumb: _thumb,
                       Id: event.Id,
                       Name: event.Name,
                       Place: event.Place,
                       Date: $scope.formatDateAddOffset(new Date(event.Date)),
                       Description: event.Description
                   };

                    $scope.content.push(eventPreview);
                };

                $scope.loadingEvents = 'success';
            }).error(function(e){
                $scope.loadingEvents = 'failure';
            });
        } else {
            $scope.loadingEvents = 'noArtistProfile';
        }

    }).error(function(error){
        $scope.loadingEvents = 'failure';
    });

  $scope.custom = {thumb:'hide-small', Id: 'hidden', Name: 'bold', Description:'fade truncate'};
  $scope.sortable = ['Name', 'Place', 'Date', 'Description'];
  $scope.thumbs = 'thumb';
  $scope.count = 3;
}]);

mpApp.directive('mdTable', function () {
  return {
    restrict: 'E',
    scope: { headers: '=', content: '=', sortable: '=', filters: '=',customClass: '=customClass',thumbs:'=', count: '=' },
    controller: function ($scope,$filter,$window) {
      var orderBy = $filter('orderBy');

      $scope.currentPage = 0;
      $scope.nbOfPages = function () {
        return Math.ceil($scope.content.length / $scope.count);
      },
        $scope.handleSort = function (field) {
          if ($scope.sortable.indexOf(field) > -1) { return true; } else { return false; }
        };
      $scope.order = function(predicate, reverse) {
        $scope.content = orderBy($scope.content, predicate, reverse);
        $scope.predicate = predicate;
      };
      $scope.order($scope.sortable[0],false);
      $scope.getNumber = function (num) {
        return new Array(num);
      };
      $scope.goToPage = function (page) {
        $scope.currentPage = page;
      };
    },
    template:
    '<table class="md-table">'+
    '<thead>'+
    '<tr class="md-table-headers-row">'+
    '<th class="md-table-header" ng-repeat="h in headers" ng-class="customClass[h.field]">'+
    '<a href ng-if="handleSort(h.field)" ng-click="reverse=!reverse;order(h.field,reverse)">{{h.name}} <span  class="md-table-caret" ng-show="reverse && h.field == predicate"><img src="https://raw.githubusercontent.com/google/material-design-icons/master/navigation/ios/ic_arrow_drop_up.imageset/ic_arrow_drop_up.png"></span><span  class="md-table-caret" ng-show="!reverse && h.field == predicate"><img src="https://raw.githubusercontent.com/google/material-design-icons/master/navigation/ios/ic_arrow_drop_down.imageset/ic_arrow_drop_down.png"></span></a>'+
    '<span ng-if="!handleSort(h.field)">{{h.name}}</span>'+
    '</th><th class="md-table-header"></th>'+
    '</tr>'+
    '</thead><tbody>'+
    '<tr class="md-table-content-row" ng-repeat="c in content | filter:filters | startFrom:currentPage*count | limitTo: count">'+
    '<td ng-repeat="h in headers" ng-if="h.field == thumbs" class="md-table-thumbs waves-effect" ng-class="customClass[h.field]" ui-sref="editEvent({id : c.Id})">'+
    '<div ng-if="h.field == thumbs" style="background-image:url({{ c.thumb }})"></div>'+
    '</td>'+
    '<td class="md-table-content waves-effect" ng-repeat="h in headers" ng-class="customClass[h.field]" ng-if="h.field != thumbs" ui-sref="editEvent({id : c.Id})">'+
    '{{c[h.field]}}'+
    '</td><td class="md-table-td-more waves-effect" ui-sref="editEvent({id : c.Id})"><md-button aria-label="edit"><img src="/public/img/pencil.png"></md-button></td>'+
    '</tr>'+
    '</tbody>'+
    '</table>'+
    '<div class="md-table-footer">'+
    '<span class="md-table-count-info">Rows count per page : <a href ng-click="goToPage(0); count=1">1</a>, <a href ng-click="goToPage(0); count=10">10</a>, <a href ng-click="goToPage(0); count=25">25</a>, <a href ng-click="goToPage(0); count=50">50</a>, <a href ng-click="goToPage(0); count=100">100</a> (current is <strong>{{count}}</strong>)</span>'+
    '<span flex></span>'+
    '<span ng-show="nbOfPages() > 1">'+
    '<a class="md-table-footer-item waves-effect btn-flat" ng-class="{\'disabled\': currentPage==0}" ng-click="currentPage = (currentPage > 0 ? currentPage-1 : 0)" aria-label="previous">'+
    '<i class="mdi-image-navigate-before"></i>'+
    '</a>'+
    '<a href ng-repeat="i in getNumber(nbOfPages()) track by $index" >'+
    '<button class="btn-flat transparent waves-effect md-table-footer-item" ng-click="goToPage($index)" aria-label="page">'+
    '<span ng-class="{ \'md-table-active-page\': currentPage==$index}">{{$index+1}}</span>'+
    '</button></a>'+
    '<a class="md-table-footer-item waves-effect btn-flat" ng-class="{\'disabled\': currentPage==nbOfPages()-1}" ng-click="currentPage = (currentPage < nbOfPages() - 1 ? currentPage+1 : currentPage)" aria-label="next">'+
    '<i class="mdi-image-navigate-next"></i>'+  // <img src="http://google.github.io/material-design-icons/hardware/svg/ic_keyboard_arrow_right_24px.svg">
    '</a></span></div>'
  }
});

mpApp.filter('startFrom',function (){
  return function (input,start) {
    start = +start;
    return input.slice(start);
  }
});

function artistIsAlreadyInLineup(id, artistsJson, isInLineup, isNotInLineup){
    var matchingId = false;

    artistsJson.some(function(artist){
        matchingId = (artist.Id === id)
        return matchingId;
    });

    matchingId === false ? isInLineup() : isNotInLineup();
};

mpApp.directive("fileread", [function () {
    return {
        scope: {
            fileread: "="
        },
        link: function (scope, element, attributes) {
            element.bind("change", function (changeEvent) {
                var reader = new FileReader();
                reader.onload = function (loadEvent) {
                    scope.$apply(function () {
                        scope.fileread = loadEvent.target.result;
                    });
                }
                reader.readAsDataURL(changeEvent.target.files[0]);
            });
        }
    }
}]);

mpApp.directive("materialboxed",[function() {
    return {
        restrict: "A",
        link: function(scope, elem, attrs) {
            $(elem).materialbox();
        }
    }
}]);

mpApp.directive("imageState", function() {
    return {
        scope: {
            uri: "@"
        },
        link: function(scope, elem, attrs) {
            if (!scope.uri){
                scope.uri = "/public/img/default-cover.png";
            }

            $(elem).cropit({ imageState: { src: scope.uri }});
        }
    }
});

mpApp.directive("cropitExport", function(){
    return {
        link: function(scope, elem, attrs){
            elem.on('click', function(e){
                var uri = $('#image-cropper').cropit('export', {
                  type: 'image/jpeg',
                  quality: .9,
                  originalSize: false
                });

                scope.uploadCover(uri);
            });
        }
    }
});

/**
* Adds a button on mouseover to either :
* - remove the photo
* - restore a photo that has been removed
*/
mpApp.directive("removablePhoto", function(){
    return {
        link: function(scope, elem, attrs){
            var tools = $('<div class="tools">');
            var deleteButton = $('<a class="tool btn-floating btn-large waves-effect white"><i class="mdi-action-delete black-text">');
            var restoreButton = $('<a class="tool btn-floating btn-large waves-effect white"><i class="mdi-content-undo black-text">');

            deleteButton.on('click', function(e){
                scope.deletePhoto(scope.$index);
                elem.append('<div class="deleted"><span class="deleted">X</span>');

                deleteButton.hide();
                restoreButton.show();
                return false;   // prevent other click functions
            });

            restoreButton.on('click', function(e){
                scope.restorePhoto(scope.$index);
                elem.children('.deleted').remove();

                restoreButton.hide();
                deleteButton.show();
                return false;
            });

            tools.append(deleteButton);
            tools.append(restoreButton);
            restoreButton.hide();
            tools.hide();   // hidden by default
            elem.append(tools);

            elem.on('mouseover', function(e){
                if (!elem.hasClass('active')){
                    tools.show();
                }
            });

            elem.on('mouseleave', function(e){
                tools.hide();
            });
        }
    }
});

mpApp.directive("tooltipped", function(){
    return {
        restrict: 'C',
        link: function(scope, elem, attrs){
            $('.tooltipped').tooltip({delay: 1});

            elem.on('click', function(){
                // $('.material-tooltip').remove();
                elem.trigger('mouseleave');
                elem.css({'opacity' : 0});
            });
        }
    }
});

mpApp.directive("modalTrigger", function(){
    return {
        restrict: 'C',
        link: function(scope, elem, attrs){
            elem.leanModal({
                in_duration: 250,
                out_duration: 250,
                opacity: 0.3,
                complete: function(){
                    elem.css({'opacity' : 1});
                }
            });
        }
    }
});

mpApp.directive("dateTimePicker", function(){
    return {
        scope: {
            timestamp: '=ngModel',
            min: '@'
        },
        link: function(scope, elem, attrs){
            var values = {
                value: scope.timestamp,
                validateOnBlur: false,
                // format: "Y\-m\-d\\TH\:i\:s\.000\\Z" // Example: 2015-04-22T00:15:00.000Z
                format: "Y\/m\/d\,\ H\:i"
            }

            scope.$watch('min', function(newValue, oldValue){
                if (scope.min){
                    values = {
                        value: scope.timestamp,
                        validateOnBlur: false,
                        minDate: scope.min.substr(0, scope.min.indexOf(',')),
                        formatDate:"Y\/m\/d",
                        // format: "Y\-m\-d\\TH\:i\:s\.000\\Z", // Example: 2015-04-22T00:15:00.000Z
                        format: "Y\/m\/d\,\ H\:i",
                        onShow: function(ct){
                            var selectedDateFormated = (ct.getFullYear() + '/'
                            + (ct.getMonth() >= 9 ? (ct.getMonth() + 1) : "0" + (ct.getMonth() + 1)) + '/'
                            + (ct.getDate() > 9 ? ct.getDate() : "0" + ct.getDate()));
                            var minDateFormated = scope.min.substr(0, scope.min.indexOf(','));

                            if(selectedDateFormated === minDateFormated){
                                this.setOptions({
                                    minTime: scope.min.substr(scope.min.indexOf(' ') + 1, 5),
                                    formatTime: "H\:i"
                                });
                            } else {
                                this.setOptions({
                                    minTime: '00:00'
                                });
                            }
                        },
                        onChangeDateTime: function(ct, $input){
                            var selectedDateFormated = (ct.getFullYear() + '/'
                            + (ct.getMonth() >= 9 ? (ct.getMonth() + 1) : "0" + (ct.getMonth() + 1)) + '/'
                            + (ct.getDate() > 9 ? ct.getDate() : "0" + ct.getDate()));
                            var minDateFormated = scope.min.substr(0, scope.min.indexOf(','));

                            if(selectedDateFormated === minDateFormated){
                                this.setOptions({
                                    minTime: scope.min.substr(scope.min.indexOf(' ') + 1, 5),
                                    formatTime: "H\:i"
                                });
                            } else {
                                this.setOptions({
                                    minTime: '00:00'
                                });
                            }
                        }
                    }
                }

                $(elem).datetimepicker(values);
            });
        }
    }
});
