$(window).load(function(){
    /**
     * prevent FOUC
     */
    $('body').show();
    /**
     * Focus form to fix a display bug where the placeholder would overlap
     * the content when set automatically
     */
    var focusLogin = function(){
        $('#username').focus();
    }();

    var showLoginError = function() {
        if ($('#error').text() !== '') {
            $('.alert-danger', $('#login')).show();
            $('#passwd').focus();
        }
    }();
    
    $('#submit').closest('form').validate({
        errorElement: 'span', //default input error message container
        errorClass: 'help-block', // default input error message class
        
        rules: {
            email: {
                required: true,
                email: true
            },
            password: {
                required: true
            }
        },
        
        messages: {
            email: {
                required: "email is required.",
                email: "invalid email address"
            },
            password: {
                required: "Password is required."
            },
        },
        
        invalidHandler: function (event, validator) { //display error alert on form submit   
            if (validator.errorList.length > 0) {
                $('#error').html(validator.errorList[0].message);
            }
            
            $('.alert-danger', $('#login')).show();
        },
        
        success: function (label) {
            $('.alert-danger', $('#login')).hide();
        },
        
        errorPlacement: function (error, element) {
            error.insertAfter(element.closest('.input-icon'));
        },
        
        submitHandler: function (form) {
            form.submit();
        }
    });

    $('#submit').on('click', function () {
        $('#submit').closest('form').submit();       
    });

    $('#login input').keypress(function (e) {
        if (e.which == 13) {            
            $('#submit').closest('form').submit();
            return false;
        }
    });
});