window.fbAsyncInit = function() {
  FB.init({
    status     : true,  // get current user info immediately after init
    appId      : 359733944179587,
    cookie     : true,  // enable cookies to allow the server to access
                        // the session
    xfbml      : true,  // parse social plugins on this page
    version    : 'v2.3' // use version 2.3
  });
};

// Load the SDK asynchronously
(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  // js.src = "//connect.facebook.net/en_US/sdk/debug.js"; // unminified version to allow easier debugging
  js.src = "//connect.facebook.net/en_CA/sdk.js";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
