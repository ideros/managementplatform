﻿$(".button-collapse").sideNav();

$(document).ready(function(){
    var tabsList = $(".toggle-active");

    tabsList.find('li').each(function(){
        $(this).on('click', function(){
            $(this).addClass('active');
            $(this).siblings().removeClass('active');
        });
    });
});
